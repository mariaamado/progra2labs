package Act2;

import Act2.*;
import org.junit.Assert;
import org.junit.Test;

public class AreaAndPerimeterTest {

    @Test
    public void testTrianguloEquilatero() {
        Figura figura = new TrianguloEquilatero(5);
        Assert.assertEquals("Triángulo Equilátero: área incorrecta", figura.calcularArea(), 10.825, 0.001);
        Assert.assertEquals("Triángulo Equilátero: perímetro incorrecto", figura.calcularPerimetro(), 15, 0);
    }

    @Test
    public void testTrianguloIsosceles() {
        Figura figura = new TrianguloIsosceles(4, 6);
        Assert.assertEquals("Triángulo Isósceles: área incorrecta", figura.calcularArea(), 12, 0);
        Assert.assertEquals("Triángulo Isósceles: perímetro incorrecto", figura.calcularPerimetro(), 4 + 4 * Math.sqrt(10), 0);
    }


    @Test
    public void testTrianguloEscaleno() {
        Figura figura = new TrianguloEscaleno(3, 4, 5, 6);
        Assert.assertEquals("Triángulo Escaleno", figura.calcularArea(), 6, 0);
        Assert.assertEquals("Triángulo Escaleno", figura.calcularPerimetro(), 14, 0);
    }

    @Test
    public void testRectangulo() {
        Figura figura = new Rectangulo(4, 6);
        Assert.assertEquals("Rectángulo", figura.calcularArea(), 24, 0);
        Assert.assertEquals("Rectángulo", figura.calcularPerimetro(), 20, 0);
    }

    @Test
    public void testCirculo() {
        Figura figura = new Circulo(3);
        Assert.assertEquals("Círculo", figura.calcularArea(), Math.PI * 9, 0);
        Assert.assertEquals("Círculo", figura.calcularPerimetro(), 2 * Math.PI * 3, 0);
    }

    @Test
    public void testCuadrado() {
        Figura figura = new Cuadrado(5);
        Assert.assertEquals("Cuadrado", figura.calcularArea(), 25, 0);
        Assert.assertEquals("Cuadrado", figura.calcularPerimetro(), 20, 0);
    }
}