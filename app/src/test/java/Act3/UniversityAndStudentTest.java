package Act3;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.List;
import Act3.Student;
import Act3.University;


public class UniversityAndStudentTest {
    private University university;
    private Student student1;
    private Student student2;
    private Student student3;

    @Before
    public void setUp() {
        university = new University();
        student1 = new Student("Gery", 1, Arrays.asList("induction"));
        student2 = new Student("Luis", 2, Arrays.asList("maths", "science"));
        student3 = new Student("Raul", 1, Arrays.asList("science"));
    }

    @Test
    public void testAddStudent() {
        university.addStudent(student1);
        university.addStudent(student2);

        List<Student> students = university.getStudents();
        Assert.assertEquals(2, students.size());
        Assert.assertTrue(students.contains(student1));
        Assert.assertTrue(students.contains(student2));
    }

    @Test
    public void testAssignCourse() {
        university.addStudent(student1);
        university.assignCourse(student1, "Database I");

        List<String> courses = student1.getCourses();
        Assert.assertEquals(2, courses.size());
        Assert.assertTrue(courses.contains("induction"));
        Assert.assertTrue(courses.contains("Database I"));
    }

    @Test
    public void testPromoteStudent() {
        university.addStudent(student2);
        university.promoteStudent(student2, 3);

        Assert.assertEquals(3, student2.getGrade());
    }

    @Test
    public void testFilterStudentsByCourse() {
        university.addStudent(student1);
        university.addStudent(student2);
        university.addStudent(student3);

        List<String> scienceStudents = university.filterStudentsByCourse("science");
        Assert.assertEquals(2, scienceStudents.size());
        Assert.assertTrue(scienceStudents.contains("Luis"));
        Assert.assertTrue(scienceStudents.contains("Raul"));
    }

}