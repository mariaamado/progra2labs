package Act1;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class QuadrantSelectionTest {

    @Test
    public void testCalcularCuadrante() {
        // Caso de prueba 1: Punto en el cuadrante 1
        int resultado1 = QuadrantSelection.calcularCuadrante(-5, 5);
        Assertions.assertEquals(1, resultado1);

        // Caso de prueba 2: Punto en el cuadrante 2
        int resultado2 = QuadrantSelection.calcularCuadrante(5, 5);
        Assertions.assertEquals(2, resultado2);

        // Caso de prueba 3: Punto en el cuadrante 3
        int resultado3 = QuadrantSelection.calcularCuadrante(-5, -5);
        Assertions.assertEquals(3, resultado3);

        // Caso de prueba 4: Punto en el cuadrante 4
        int resultado4 = QuadrantSelection.calcularCuadrante(5, -5);
        Assertions.assertEquals(4, resultado4);

        // Caso de prueba 5: Punto en el origen (0,0)
        int resultado5 = QuadrantSelection.calcularCuadrante(0, 0);
        Assertions.assertEquals(-1, resultado5);
    }
}
