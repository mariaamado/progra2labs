package Act1;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import Act1.LastFactorialDigit;

public class testLastFactorialDigit {
    @Test
    public void testCalcularFactorial(){
            // Caso de prueba 1: Factorial de 5
            long resultado1 = LastFactorialDigit.calcularFactorial(5);
            Assertions.assertEquals(120, resultado1);

            // Caso de prueba 2: Factorial de 0
            long resultado2 = LastFactorialDigit.calcularFactorial(0);
            Assertions.assertEquals(1, resultado2);

            // Caso de prueba 3: Factorial de un número negativo
            Assertions.assertThrows(IllegalArgumentException.class, () -> {
                LastFactorialDigit.calcularFactorial(-5);
            });
        }

    @Test
        public void testLastDigit() {
            // Caso de prueba 1: Último dígito de 1234567890
            long resultado1 = LastFactorialDigit.lastDigit(1234567890);
            Assertions.assertEquals(0, resultado1);

            // Caso de prueba 2: Último dígito de 987654321
            long resultado2 = LastFactorialDigit.lastDigit(987654321);
            Assertions.assertEquals(1, resultado2);

            // Caso de prueba 3: Último dígito de 0
            long resultado3 = LastFactorialDigit.lastDigit(0);
            Assertions.assertEquals(0, resultado3);
        }
    }