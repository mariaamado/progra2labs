package Act2;

public class TrianguloEscaleno extends Triangulo {
    protected double lado2;
    protected double lado3;

    public TrianguloEscaleno(double base, double altura, double lado2, double lado3) {
        super(base, altura);
        this.lado2 = lado2;
        this.lado3 = lado3;
        this.nombre = "Triángulo Escaleno";
    }

    @Override
    public double calcularPerimetro() {
        return base + lado2 + lado3;
    }
}