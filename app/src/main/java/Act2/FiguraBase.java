package Act2;

import java.text.DecimalFormat;

abstract class FiguraBase implements Figura{
    protected String nombre;

    public FiguraBase(String nombre) {
        this.nombre = nombre;
    }

    public void showDetails() {
        DecimalFormat df = new DecimalFormat("#.##");
        System.out.println("Nombre: " + nombre);
        System.out.println("Area: " + df.format(calcularArea()));
        System.out.println("Perimetro: " + df.format(calcularPerimetro()));
        System.out.println();
    }
}