package Act2;

public class TrianguloIsosceles extends Triangulo{
    public TrianguloIsosceles(double base, double altura) {
        super(base, altura);
        this.nombre = "Triángulo Isósceles";
    }

    @Override
    public double calcularPerimetro() {
        return 2 * base + calcularLado();
    }

    private double calcularLado() {
        return Math.sqrt(Math.pow(base / 2, 2) + Math.pow(altura, 2));
    }
}