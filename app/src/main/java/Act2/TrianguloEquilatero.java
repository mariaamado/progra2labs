package Act2;

public class TrianguloEquilatero extends  Triangulo{
    public TrianguloEquilatero(double lado) {
        super(lado, Math.sqrt(3) / 2 * lado);
        this.nombre = "Triángulo Equilátero";
    }

    @Override
    public double calcularPerimetro() {
        return 3 * base;
    }

}