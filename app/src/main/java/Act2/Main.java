package Act2;

public class Main {
    public static void main(String[] args){
        TrianguloEquilatero trianguloEquilatero = new TrianguloEquilatero(7);
        TrianguloIsosceles trianguloIsosceles = new TrianguloIsosceles(4, 6);
        TrianguloEscaleno trianguloEscaleno = new TrianguloEscaleno(3, 4, 4,7);
        Cuadrado cuadrado = new Cuadrado(2);
        Rectangulo rectangulo = new Rectangulo(10, 5);
        Circulo circulo = new Circulo(5);


        trianguloEscaleno.showDetails();
        trianguloEquilatero.showDetails();
        trianguloIsosceles.showDetails();
        circulo.showDetails();
        cuadrado.showDetails();
        rectangulo.showDetails();
    }
}