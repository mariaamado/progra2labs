package Act2;

public class Cuadrado extends Rectangulo{
    public Cuadrado(double lado) {
        super(lado, lado);
        this.nombre = "Cuadrado";
    }
}