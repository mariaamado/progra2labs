package Act2;

public interface Figura {
    double calcularArea();
    double calcularPerimetro();

}
