package Act2;

    public class Triangulo  extends  FiguraBase{
        protected double base;
        protected double altura;

        public Triangulo(double base, double altura) {
            super("Triángulo");
            this.base = base;
            this.altura = altura;
        }

        @Override
        public double calcularArea() {
            return (base * altura) / 2;
        }

        @Override
        public double calcularPerimetro() {
            throw new UnsupportedOperationException("No se puede calcular el perímetro de un triángulo solo con base y altura");
        }

    }

