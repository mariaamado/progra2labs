package Act3;

import java.util.ArrayList;
import java.util.List;

public class University {
    private List<Student> students;

    public University() {
        this.students = new ArrayList<>();
    }

    public void addStudent(Student student) {
        students.add(student);
    }

    public void assignCourse(Student student, String course) {
        student.addCourse(course);
    }

    public void promoteStudent(Student student, int newGrade) {
        student.setGrade(newGrade);
    }

    public List<String> filterStudentsByCourse(String course) {
        List<String> filteredStudents = new ArrayList<>();
        for (Student student : students) {
            if (student.getCourses().contains(course)) {
                filteredStudents.add(student.getName());
            }
        }
        return filteredStudents;
    }

    public void printStudentDetails() {
        for (Student student : students) {
            System.out.println("Name: " + student.getName() +
                    ", Grade: " + student.getGrade() +
                    ", Courses: " + String.join(", ", student.getCourses()));
        }
    }
    public List<Student> getStudents() {
        return students;
    }
}