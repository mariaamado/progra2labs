package Act3;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        University university = new University();

        // Agregar estudiantes
        university.addStudent(new Student("Gery", 1, new ArrayList<>(List.of("induction"))));
        university.addStudent(new Student("Luis", 2, new ArrayList<>(List.of("maths", "science"))));
        university.addStudent(new Student("Raul", 1, new ArrayList<>(List.of("science"))));
        university.addStudent(new Student("Luz", 3, new ArrayList<>(List.of("maths", "Logica"))));
        university.addStudent(new Student("juan", 5, new ArrayList<>(List.of("maths", "Database I"))));
        university.addStudent(new Student("yeiyei", 1, new ArrayList<>(List.of("maths", "Logica", "Progra"))));
        university.addStudent(new Student("Mapu", 8, new ArrayList<>(List.of("maths", "Database I"))));
        university.addStudent(new Student("Alexis", 4, new ArrayList<>(List.of("maths", "Logica"))));

        // Asignar un nuevo curso a un estudiante
        university.assignCourse(university.getStudents().get(3), "Database I");
        university.assignCourse(university.getStudents().get(0), "Database I");

        // Promover a un estudiante
        university.promoteStudent(university.getStudents().get(1), 3);

        // Listar los estudiantes de un curso
        List<String> scienceStudents = university.filterStudentsByCourse("Logica");
        System.out.println("Students in science: " + scienceStudents);

        // Listar todos los estudiantes con formato
        university.printStudentDetails();
    }

}
