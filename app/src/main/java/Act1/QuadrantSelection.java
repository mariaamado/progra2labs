package Act1;

import java.util.Scanner;

public class QuadrantSelection {
    //1. pedir valor para X y Y
    //2. validar valor para x y y dentro del rango
    //3. validar los valores para asignar un  cuadrante
    //4. mostrar al usuario la respuesta


    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ingrese el valor de x: ");
        int x = scanner.nextInt();
        System.out.println("Ingrese el valor de y");
        int y = scanner.nextInt();

        if (x > -1000 && x < 1000 && y > -1000 && y < 1000){
            int cuadrante = calcularCuadrante(x, y);
            if (cuadrante < 0){
                System.out.println("?");
            } else {
                System.out.println(cuadrante);
            }

        } else {
            System.out.println("Rango inválido");
        }
    }
    public static int calcularCuadrante(int x, int y){
        if(x !=0 && y !=0){
            if (x < 0 && y > 0){
                return 1;
            } else if(x > 0 && y > 0){
                return 2;
            } else if(x < 0 && y < 0) {
                return 3;
            } else if(x > 0 && y < 0){
                return 4;
            }
        }
        return -1 ;
    }
}
