 package Act1;

         import java.util.Scanner;

public class LastFactorialDigit {
    //1.pedir los casos de prueba
    //2.comprobar los valores estan en el rango
    //3.retornar el numero y ultimo digito
        //3.1 de lo contrario mostrar por mensaje el rango
    //4.realizar el calculo de factorial y mostrar el ultimo digito
    public static void main(String[] arg){
        Scanner scan = new Scanner(System.in);
        System.out.println("digitar los casos de prueba");
        int T = scan.nextInt();

        if (T < 10 && T > 1){
            for (int i = 0; i < T; i++){
                int num = scan.nextInt();
                long lastDig = lastDigit(calcularFactorial(num));

                System.out.println(num +"  -  " + lastDig);
            }
        }else
            throw new IllegalArgumentException("Los casos de uso deben estar entre 1 < T < 10");
    }
    public static long calcularFactorial(int numero){
        if (numero < 0) {
            throw new IllegalArgumentException("el numero tiene que ser mayor o igual de cero.");
        }
        long factorial = 1;
        for (int i = 1; i <= numero; i++) {
            factorial *= i;
        }
        return factorial;
    }
    public static long lastDigit(long num ){
        long ultDigit = Math.abs(num % 10);
        return ultDigit;
    }
}