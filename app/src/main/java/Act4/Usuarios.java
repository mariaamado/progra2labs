package Act4;

public class Usuarios extends Persona {
    private String contrasena;
    private String rol;

    public Usuarios(String cedula, String email, String nombre, String contrasena, String rol) {
        super(cedula, email, nombre);
        this.contrasena = contrasena;
        this.rol = rol;
    }

    public Usuarios(String cedula, String nombre, String contrasena, String rol) {
        super(cedula, nombre);
        this.contrasena = contrasena;
        this.rol = rol;
    }

    public Usuarios() {
    }

    public String getCedula() {
        return getCedula();
    }

    public String getNombre() {
        return getNombre();
    }

    public String getEmail(){
        return getEmail();
    }

    public String getContrasena() {
        return contrasena;
    }

    public String getRol() {
        return rol;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public void setNombre(String nombre){
        this.no = nombre;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }



    public void buscarEventos(){

    }

    public void comrparEntradas(){

    }

    public void elegirAsientos(){

    }


}
