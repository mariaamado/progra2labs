package Act4;

public class Persona {
    private String cedula;
    private String email;
    private String nombre;

    public Persona(String cedula, String email, String nombre) {
        this.cedula = cedula;
        this.email = email;
        this.nombre = nombre;
    }

    public Persona(String cedula, String nombre) {
        this.cedula = cedula;
        this.nombre = nombre;
    }


    public Persona() {
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
