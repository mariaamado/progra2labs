package Act4;

import javax.naming.PartialResultException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Evento {
    private String nombre;
    private String lugar;
    private String fecha;
    private Artista artista;

    public Evento(String nombre, String lugar, String fecha, Artista artista){
        this.nombre = nombre;
        this.lugar = lugar;
        this.fecha = fecha;
        this.artista = artista;
    }

    public String getNombre() {
        return nombre;
    }

    public String getLugar() {
        return lugar;
    }

    public String getFecha() {
        return fecha;
    }

    public Artista getArtista() {
        return artista;
    }

    public void setNombre() {
        this.nombre = nombre;
    }

    public void setLugar() {
        this.lugar = lugar;
    }

    public void setFecha() {
        this.fecha = fecha;
    }

    public Date paraFecha() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Ingresa fecha (dd/MM/yyyy): ");
        String paraFecha = scanner.nextLine();

        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        Date fecha = null;

        try {
            fecha = formato.parse(paraFecha);
        } catch (ParseException e) {
            System.out.println("Error parseando fecha");
            e.printStackTrace();
        }
        return fecha;
    }


    public void mostrarDetalles(){
        System.out.println("Detalle del evento");
        System.out.println("Nombre: " + this.nombre);
        System.out.println("Lugar: " + this.lugar);
        System.out.println("Fecha: " + this.fecha);
        System.out.println("Artista: "+ this.artista);
    }


}



