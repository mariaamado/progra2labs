package Act4;

public class Artista {
    private String nombre;
    private String album;

    public Artista(String nombre, String album){
        this.nombre = nombre;
        this.album = album;
    }

    public String getNombre() {
        return nombre;
    }

    public String getAlbum() {
        return album;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public void detalleArtista(){
        System.out.println("Detalle del artista");
        System.out.println("Nombre del artista: " + this.nombre);
        System.out.println("Nombre del album: " + this.album);
    }
}
