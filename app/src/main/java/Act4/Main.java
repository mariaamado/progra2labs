package Act4;

public class Main {
    public static void main(String[] args) {

    }
    public void crearEventos() {

        Artista rufusDuSol = new Artista("Rufus du sol", "Surrender");
        Artista theRockers = new Artista("The rockers", "Night");
        Artista jalaUniversity = new Artista("Jala University", "Edicion 1");
        Artista francoEscamilla= new Artista("Franco Escamilla", "Edicion 1");

        Evento evento1 = new Evento("Afterlife", "Medellin", "08/08/2023", rufusDuSol);
        Evento evento2 = new Evento("Concierto de rock", "Bogota", "15/08/2023", theRockers);
        Evento evento3 = new Evento("Conferencia Tecnologica", "Bucaramanga", "20/08/2023", jalaUniversity);
        Evento evento4 = new Evento("Concurso: Teatral", "Cali", "25/09/2023", francoEscamilla);
    }


}
